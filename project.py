# -*- coding: utf-8 -*-
__author__ = "Kevin Boeuf, Tony Hennequin, Florentin Molimard"

import glob
import math
import random
from collections import defaultdict
import operator

# dossier source des fichiers
LOCALISATIONFICHIERS = "./corpus/txt/"

# dossier contenant la stop liste
LOCALISATIONSTOPLIST = "./corpus/"

# nombre de paquets
NBPAQUETS = 20

# nombre de plus proches voisins
K = 20;

# liste des paquets
paquets = list()

# liste des fichiers
listeFichiers = list()

# liste des auteurs des textes
dictionnaireAuteurs = defaultdict(lambda: [])

# fichier inversé du corpus de texte (base d'apprentissage)
invertedFile = defaultdict(lambda: defaultdict(lambda: 0))

# fichier inversé des requêtes
invertedFileReq = defaultdict(lambda: defaultdict(lambda: 0))

# résultats du système
results = dict()


# Initialise la liste des fichiers
def initListe():
    for file_text in glob.iglob(LOCALISATIONFICHIERS + "*.txt"):
        nbCaracteresLocalisation = len(LOCALISATIONFICHIERS)
        file_text = file_text[nbCaracteresLocalisation:]
        listeFichiers.append(file_text)
        dictionnaireAuteurs[file_text].append(getAuthor(file_text))
        dictionnaireAuteurs[file_text].append(0)  # contiendra la norme du fichier


# Extrait un paquet de listeFichiers
def genererUnPaquet(tableau, nbCases):
    paquet = random.sample(tableau, nbCases)
    for i in paquet:
        listeFichiers.remove(i)
    return paquet


# Génère "nbPaquets" paquets
def genererPaquets(nbPaquets):
    listePaquets = {}
    nbTextes = len(listeFichiers)
    nbTextesParPaquet = int(nbTextes / (nbPaquets))
    # On génère nbPaquets aléatoirement
    for i in range(0, nbPaquets):
        paquet = genererUnPaquet(listeFichiers, nbTextesParPaquet)
        listePaquets[i] = paquet
    # Les éléments restants dans le tableau listeFichiers s'ajoutent au dernier paquet
    for i in listeFichiers:
        listePaquets[nbPaquets - 1].append(i)
    return listePaquets


# Affichage du contenu d'une liste
def afficheListe(liste):
    for i in liste:
        print(i)


# Retourne l'auteur à partir du nom du fichier
def getAuthor(nomFichier):
    return nomFichier[:-5] # enlève ".txt" aux noms des fichiers


# Remplace les , . et ; par des espaces puis sépare la chaine obtenue selon les espaces
def tokenize(line):
    return line.replace(",", " ").replace(".", " ").replace(";", " ").split()


# Permet de filtrer une liste de mots
def stopWords(invFile):
    print("Application des stop words")
    stopList = open(LOCALISATIONSTOPLIST + "common_words.total_fr.txt", "r")
    for i in stopList.read().splitlines():
        # print i
        if i in invFile:
            del invFile[i]


# Calcul du TF, le poids local d'un mot
def calculTF(paquetsUtilise, invFile):
    print("Tokenization et calcul du TF...")
    # Pour chaque index de paquet
    for i in paquetsUtilise:
        # Pour chaque fichier d'un paquet donné
        for j in paquetsUtilise[i]:
            # Pour chaque ligne d'un fichier d'un paquet donné
            for line in open(LOCALISATIONFICHIERS + j):
                for word in tokenize(line):
                    invFile[word][j] += 1


# calcule l'IDF de chaque mot d'une liste de documents
# @listDoc est la liste des documents
# @invFile contient le poids de chaque mot
def calculIdf(listDoc, invFile):
    N = 0
    for key in listDoc:# nombre de documents avec lesquels on travaille
        N += len(listDoc[key])
    for i in invFile:
        n = len(invFile[i])  # nombre de documents qui contiennent ce mot
        if N / n > 0:  # test dans le cas d'une division par 0
            invFile[i]["idf"] = math.log10(N / n)
        else:
            invFile[i]["idf"] = 1.0


# calcule les valeurs de normalisation
def calculNorme(dicoAuteurs, invFile):
    for word in invFile:
        for doc in invFile[word]:
            if len(dicoAuteurs[doc]) == 0:
                dicoAuteurs[doc].append('')
                dicoAuteurs[doc].append(0) #doit contenir la norme pour ce document
            dicoAuteurs[doc][1] += (invFile[word][doc] * invFile[word]["idf"]) ** 2
            if dicoAuteurs[doc][1] == 0:
                dicoAuteurs[doc][1] = 1
            else:
                dicoAuteurs[doc][1] = 1 / dicoAuteurs[doc][1]


# calcule le poids normalisé d'un mot
def nW(tf, idf, n):
    return tf * idf * n


# calcule le poids normalisé pour chaque mot d'une collection de mots
def normalizedWeight(collection, textList, invFile):
    print("Calcul du poids normé de chaque mot...")

    print("\t-calcul de l'idf...")
    calculIdf(collection, invFile)

    print("\t-calcul de la norme...")
    calculNorme(textList, invFile)

    print("\t-calcul du poids normalisé...")
    for word in invFile:
        for doc in invFile[word]:
            invFile[word][doc] = nW(invFile[word][doc], invFile[word]["idf"], textList[doc][1])

# extraie les poids de chaque mot d'un document
def extractWeight(nomFichier, invFile):
    f = dict()
    for word in invFile:
        f[word] = 1
        for doc in invFile[word]:
            if nomFichier == doc:
                f[word] = invFile[word][doc]
    return f

# calcule le cosinus (distance) entre un document source et un document requête
def cosinus(doc, req):
    numerateur = 0
    denominateur = 1
    denom1 = 0
    denom2 = 0

    for key in doc:
        if (req.get(key) != None):
            numerateur = numerateur + (doc[key] * req[key])
            denom1 = denom1 + (doc[key] * doc[key])
            denom2 = denom2 + (req[key] * req[key])

    denominateur = math.sqrt(denom1*denom2)
    return numerateur / denominateur

# Test du paquet de requête contre tous les autres
def processComp(paquetRequete, paquetRessource, invFileReq, invFileRes):
    print("\tCalculs sur le paquet de tests")
    for req in paquetRequete[0]:
        q = extractWeight(req, invFileReq)

        c = []
        print("\tCalcul du cosinus pour paquet = "+req)
        for paquet in paquetRessource:
            for doc in paquetRessource[paquet]:
                d = [doc, cosinus(extractWeight(doc, invFileRes), q)]
                c.append(d)
        sortedTab = sorted(c, key=lambda item: item[1], reverse=True)
        print(sortedTab)
        results[req] = sortedTab;

# détermine le taux de précision à partir des résultats du système
def calculPrecision(result):
    tauxReussite = 0
    score = 0
    taillePaquetRequete = len(paquetTest[0])
    for doc in result:
        # pour chaque document requete
        occurences = 0
        auteurRequete = dictionnaireAuteurs[doc][0]
        auteursExtract = dict()
        for i in range(0, K):
            auteur = dictionnaireAuteurs[result[doc][i][0]][0]
            if(auteur in auteursExtract):
                auteursExtract[auteur] = auteursExtract[auteur] + 1
            else:
                auteursExtract[auteur] = 1
        maxAuteur = max(auteursExtract.items(), key=operator.itemgetter(1))[0]
        if(maxAuteur == auteurRequete):
            score = score + 1

    tauxReussite = score / taillePaquetRequete
    print ("Le taux de réussite est de : {0}".format(tauxReussite))

###################
# Run the program #
###################
initListe()

print("============== Generation des paquets ==============")
paquets = genererPaquets(NBPAQUETS)
# print paquets
# Extraction du paquet 0 dans le paquet test
pT = paquets[NBPAQUETS - 1]
paquetTest = {}
paquetTest[0] = pT
del (paquets[NBPAQUETS - 1])
print("Paquet de requête: {0}\n\n".format(paquetTest))

# traiter les paquets de comparaison/ressources
print("============== Traitement des paquets d'apprentissage ==============")
calculTF(paquets, invertedFile)
print("Nombre total de mots avant application des stop words : {0}".format(len(invertedFile)))
stopWords(invertedFile)
print("Nombre total de mots après application des stop words : {0}".format(len(invertedFile)))
normalizedWeight(paquets, dictionnaireAuteurs, invertedFile)
print("\n\n")

# traiter les paquets de test
print("============== Traitement du paquet de requête ==============")
calculTF(paquetTest, invertedFileReq)
print("Nombre total de mots dans le dictionnaire de fichier inversé du paquet de requête : {0}".format(len(invertedFileReq)))
stopWords(invertedFileReq)
print("Nombre de mots dans le dictionnaire de fichier inversé du paquet de requête après stop list : {0}".format(len(invertedFileReq)))
normalizedWeight(paquetTest, dictionnaireAuteurs, invertedFileReq)
print("\n\n")

# Comparaison entre ressources et requete
print("============== Détermination de l'auteur des textes du paquet de requête ==============")
processComp(paquetTest, paquets, invertedFileReq, invertedFile)

print(results)

print("calcul du taux de précision")

calculPrecision(results)

print("Done")

